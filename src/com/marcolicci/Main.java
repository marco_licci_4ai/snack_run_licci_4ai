package com.marcolicci;

public class Main {

    public static boolean raceEnded;

    public static void main(String[] args) {
        raceEnded = false;

        new Nephew("Qui").start();
        new Nephew("Quo").start();
        new Nephew("Qua").start();
    }
}
