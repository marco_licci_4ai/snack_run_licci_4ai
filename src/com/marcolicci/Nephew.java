package com.marcolicci;

/**
 * Created by marco on 21/03/2017.
 */
public class Nephew extends Thread {
    public Nephew(String name) {
        super(name);
    }

    @Override
    public void run(){
        int distance = 0;
        do {
            distance += (int)(Math.random() * 10);
            yield();
        } while(distance < 10 && !Main.raceEnded);
        if (!Main.raceEnded) {
            Main.raceEnded = true;
            System.out.println(getName() + " won!");
        }
    }
}
